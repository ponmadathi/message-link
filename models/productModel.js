

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var productSchema = new Schema({
    urlOrMsgType: { type: String, required: true },
    urlOrMsg: { type: String, required: true },
    url: { type: String , required: true },
    timer: { type: String , required: true },
    timestamp: { type: String , required: true },
});

module.exports= mongoose.model('urlModel', productSchema);