
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var loginSchema = new Schema({
    userEmail: { type: String, required: true },
    password: { type: String, required: true }
});

module.exports= mongoose.model('loginModel', loginSchema);



