const express = require('express')
const router = express.Router()
const loginRouter = require('./login')

router.get('/', authenticate, (req, res) => {
    res.render('pages/about', { name: req.session.email });
})


function authenticate(req, res, next) {
    if (req.session && req.session.email) {
        return next();
    }
    else {
        console.log("session data in else home get", req.session);
        res.redirect('/login');
    }
}

module.exports = router